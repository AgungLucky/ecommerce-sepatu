<?php
            
            include('../include/config.php');
            $link=koneksi_db(); 
            $sql="select 
            		barang.id_barang,
            		barang.nama_barang,
            		barang.harga,
            		transaksi.id_transaksi,
            		transaksi.tgl,
            		detail_transaksi.qty,
            		detail_transaksi.total,
            		transaksi.total_harga

            	 from 
            	 	detail_transaksi 
            	 	inner join transaksi on transaksi.id_transaksi = detail_transaksi.id_transaksi
            	 	inner join barang on barang.id_barang = detail_transaksi.id_barang
            	 where
            	 	transaksi.id_transaksi = '".$_GET['id_transaksi']."'
            	  "; 

            $res=mysqli_query($link,$sql); 
            $banyakrecord=mysqli_num_rows($res); 
                if($banyakrecord>0){  
            
            ?> 

       <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i> Data Barang</div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Kode Transaksi</th>
                  <th>Tanggal</th>
                  <th>Kode Barang</th>
                  <th>Nama Barang</th>
                  <th>Harga Satuan</th>
                  <th>Jumlah</th>
                  <th>Total</th>
                </tr>
              </thead>
              
              <tbody>
              <?php
              
                  $i=0; 
                  while( $data=mysqli_fetch_array($res)){ 
                  $i++;
              		$total = $data['total_harga'];
              ?>
                <tr>
                  <td><?php echo $i;?></td>
                  <td><?php echo $data['id_transaksi'];?></td>
                  <td><?php echo $data['tgl'];?></td>
                  <td><?php echo $data['id_barang'];?></td>
                  <td><?php echo $data['nama_barang'];?></td>
                  <td><?php echo $data['harga'];?></td>
                  <td><?php echo $data['qty'];?></td>
                  <td><?php echo $data['total'];?></td>
                </tr>
              </tbody>                                                        
              <?php
            }
              ?>
            </table>
            <h3 class="pull-right"> Total : <?=$total?> </h3>
            <?php
              } else echo "Tidak ada data pada tabel Kategori."; 
            ?>


          </div>
        </div>
        <div class="card-footer small text-muted"></div>
      </div>