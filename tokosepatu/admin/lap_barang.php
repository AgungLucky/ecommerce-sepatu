<?php ob_start(); ?>
<html>
<head>
  <title>Cetak PDF</title>
    
   <style>
   table {border-collapse:collapse; table-layout:fixed;width: 630px;}
   table td {word-wrap:break-word;width: 20%;}
   </style>
</head>
<body>
<table border="0">
  <tr>
    <td rowspan="4" ><img src="gambar/sepatu2.jpg" width="150" height="80"></td>
        <td><b><font face="Engravers MT" size="1">TOKO SEPATU HAYU GAWE </font></b></td>
        <td></td>
        <td></td>
  </tr>
  <tr>
        <td>Telepon : 08123456789</td>
        <td></td>
        <td></td>
  </tr>
  <tr>
        <td>Jln. Nin Aja Dulu No 1</td>
        <td></td>
        <td></td>
  </tr>
  <tr>
        <td>www.hayugawe.com</td>
        <td></td>
        <td></td>
  </tr>
</table>
<p><img src="gambar/line.JPG" width="100%" height="10px"></p>
  
<h1 style="text-align: center;">Data Barang</h1>
<table border="1" width="100%">
<tr>
  <th>No</th>
  <th>Kode Barang</th>
  <th>Nama Barang</th>
  <th>Stok</th>
  <th>Harga</th>
</tr>
<?php
// Load file koneksi.php
include "../include/config.php";
$link = koneksi_db();
$query = "SELECT * FROM barang"; // Tampilkan semua data gambar
$sql = mysqli_query($link, $query); // Eksekusi/Jalankan query dari variabel $query
$row = mysqli_num_rows($sql); // Ambil jumlah data dari hasil eksekusi $sql
 
if($row > 0){ // Jika jumlah data lebih dari 0 (Berarti jika data ada)
    $i=0;
    while($data = mysqli_fetch_array($sql)){ // Ambil semua data dari hasil eksekusi $sql
    $i++;
        echo "<tr>";
        echo "<td>".$i."</td>";
        echo "<td>".$data['id_barang']."</td>";
        echo "<td>".$data['nama_barang']."</td>";
        echo "<td>".$data['stok']."</td>";
        echo "<td>".$data['harga']."</td>";
        echo "</tr>";
    }
}else{ // Jika data tidak ada
    echo "<tr><td colspan='4'>Data tidak ada</td></tr>";
}
?>
</table>

</body>
</html>
<?php
$html = ob_get_contents();
ob_end_clean();
        
require_once('../html2pdf/html2pdf.class.php');
$pdf = new HTML2PDF('P','A4','en');
$pdf->WriteHTML($html);
$pdf->Output('Data Barang.pdf', 'D');
?>