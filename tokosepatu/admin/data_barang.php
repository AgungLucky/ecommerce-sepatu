<?php
/*
  session_start();
  if(($_SESSION['sudahlogin']==true) && ($_SESSION['username']!=""))
  {
*/
?>  

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>SB Admin - Start Bootstrap Template</title>
  <!-- Bootstrap core CSS-->
  <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Page level plugin CSS-->
  <link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
  <!-- Custom styles for this template-->
  <link href="../css/sb-admin.css" rel="stylesheet">
</head>


<body class="fixed-nav sticky-footer bg-dark" id="page-top">
  <!-- Navigation-->
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
    <a class="navbar-brand" href="halaman_utama.php">Toko Sepatu</a>
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav navbar-sidenav" id="exampleAccordion">
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Dashboard">
          <a class="nav-link" href="halaman_utama.php">
            <i class="fa fa-fw fa-dashboard"></i>
            <span class="nav-link-text">Dashboard</span>
          </a>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Charts">
          <a class="nav-link" href="transaksi.php">
            <i class="fa fa-fw fa-area-chart"></i>
            <span class="nav-link-text">Transaksi Baru</span>
          </a>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Charts">
          <a class="nav-link" href="daftar_transaksi.php">
            <i class="fa fa-fw fa-area-chart"></i>
            <span class="nav-link-text">Daftar Transaksi</span>
          </a>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Charts">
          <a class="nav-link" href="data_barang.php">
            <i class="fa fa-fw fa-area-chart"></i>
            <span class="nav-link-text">Data Barang</span>
          </a>
        </li>
        
      <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Components">
          <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseComponents" data-parent="#exampleAccordion">
            <i class="fa fa-fw fa-area-chart"></i>
            <span class="nav-link-text">Laporan Transaksi</span>
          </a>
          <ul class="sidenav-second-level collapse" id="collapseComponents">
            <li>
              <a href="laporan_transaksi_bulan.php">Cetak Laporan</a>
            </li>
            <li>
              <a href="laporan_transaksi_global.php">Laporan Keseluruhan</a>
            </li>
          </ul>
        </li>



     

        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Link">
          <a class="nav-link" href="#">          
          </a>
        </li>
      </ul>
      <ul class="navbar-nav sidenav-toggler">
        <li class="nav-item">
          <a class="nav-link text-center" id="sidenavToggler">
            <i class="fa fa-fw fa-angle-left"></i>
          </a>
        </li>
      </ul>
      <ul class="navbar-nav ml-auto">
          <li class="nav-item">
          <form class="form-inline my-2 my-lg-0 mr-lg-2">
            <div class="input-group">
              <input class="form-control" type="text" placeholder="Search for...">
              <span class="input-group-btn">
                <button class="btn btn-primary" type="button">
                  <i class="fa fa-search"></i>
                </button>
              </span>
            </div>
          </form>
        </li>
        
        <li class="nav-item">
          <a class="nav-link" data-toggle="modal" data-target="#exampleModal">
            <i class="fa fa-fw fa-sign-out"></i>Logout</a>
        </li>

      </ul>
    </div>
  </nav>

  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
     
      <!-- Icon Cards-->

      <!-- Area Chart Example-->
      <div class="card mb-3">
        <div class="card-header">
            <h1 align="center"> Data Barang </h1>
            <br>
            <hr>

            <!-- button -->
          <div class="container-fluid">
                  
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#databarang" data-whatever="@mdo">Tambah Data Barang</button> <a href="lap_barang.php"><button type="button" class="btn btn-primary" data-toggle="modal" data-whatever="@mdo">Cetak Data Barang</button></a>

<div class="modal fade" id="databarang" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Tambah Data Barang </h5>
      </div>
      <div class="modal-body">

        <form action="proses_tambah_barang.php" method="POST">
          <div class="form-group">
            <label for="recipient-name" class="form-control-label"> Kode Barang </label>
            <input type="text" class="form-control" id="id_barang" name="id_barang">
          </div>

          <div class="form-group">
            <label for="recipient-name" class="form-control-label"> Nama Barang </label>
            <input type="text" class="form-control" id="nama_barang" name="nama_barang">
          </div>

          <div class="form-group">
            <label for="recipient-name" class="form-control-label"> Stok </label>
            <input type="text" class="form-control" id="stok" name="stok">
          </div>

          <div class="form-group">
            <label for="recipient-name" class="form-control-label"> Harga </label>
            <input type="text" class="form-control" id="harga" name="harga">
          </div>
          
        

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
        <button type="submit" class="btn btn-primary">Tambah</button>
      </div>
    </div>
    </form>
  </div>
</div>
           
            <!-- ############### -->

             <!-- Tabel Data Barang-->
             <br>
             <br>

            <?php
            include('../include/config.php');
            $link=koneksi_db(); 
            $sql="select * from barang"; 

            $res=mysqli_query($link,$sql);
            //var_dump($res); 
            $banyakrecord=mysqli_num_rows($res); 
                if($banyakrecord>0){  
            ?> 

             <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i> Data Barang</div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Kode Barang</th>
                  <th>Nama Barang</th>
                  <th>Stok</th>
                  <th>Harga</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              
              
              <tbody>
              <?php
                  $i=0; 
                  while( $data=mysqli_fetch_array($res)){ 
                  $i++;
              ?>
                <tr>
                  <td><?php echo $i;?></td>
                  <td><?php echo $data['id_barang'];?></td>
                  <td><?php echo $data['nama_barang'];?></td>
                  <td><?php echo $data['stok'];?></td>
                  <td><?php echo $data['harga'];?></td>
                  <td>
                  <a href="edit_barang.php?id=<?php echo $data['id_barang'];?>"> <button type="button" class="btn btn-second" data-whatever="@mdo">Ubah</button></a>               
                  <a href="hapus_barang.php?id=<?php echo $data['id_barang'];?>" onclick="return confirm('Apakah anda yakin akan menghapus data ini?')"> <button type="button" class="btn btn-second" data-whatever="@mdo">hapus</button></a>
                  </td>
                </tr>
                  <?php
            }
              ?>
              </tbody>                                                        
            
            </table>

            <?php
               } else echo "Tidak ada data pada tabel Kategori."; 
            ?>


          </div>
        </div>
        <div class="card-footer small text-muted"></div>
      </div>
    </div>

    </div>
  </div>  
      </div>
     
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
    <footer class="sticky-footer">
      <div class="container">
        <div class="text-center">
          <small>Copyright © Your Website 2017</small>
        </div>
      </div>
    </footer>
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fa fa-angle-up"></i>
    </a>
    <!-- Logout Modal-->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Apa Anda Yakin untuk Keluar? </h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">Tekan "Ya" untuk Keluar</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Tidak</button>
            <a class="btn btn-primary" href="index.php">Ya</a>
          </div>
        </div>
      </div>
    </div>
    <!-- Bootstrap core JavaScript-->
    <script src="../vendor/jquery/jquery.min.js"></script>
    <script src="../vendor/popper/popper.min.js"></script>
    <script src="../vendor/bootstrap/js/bootstrap.min.js"></script>
    <!-- Core plugin JavaScript-->
    <script src="../vendor/jquery-easing/jquery.easing.min.js"></script>
    <!-- Page level plugin JavaScript-->
    <script src="../vendor/chart.js/Chart.min.js"></script>
    <script src="../vendor/datatables/jquery.dataTables.js"></script>
    <script src="../vendor/datatables/dataTables.bootstrap4.js"></script>
    <!-- Custom scripts for all pages-->
    <script src="../js/sb-admin.min.js"></script>
    <!-- Custom scripts for this page-->
    <script src="../js/sb-admin-datatables.min.js"></script>
    <script src="../js/sb-admin-charts.min.js"></script>
  </div>
</body>

</html>
