<?php
/*
  session_start();
  if(($_SESSION['sudahlogin']==true) && ($_SESSION['username']!=""))
  {
*/
?>  

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>SB Admin - Start Bootstrap Template</title>
  <!-- Bootstrap core CSS-->
  <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Page level plugin CSS-->
  <link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
  <!-- Custom styles for this template-->
  <link href="../css/sb-admin.css" rel="stylesheet">
</head>


<body class="fixed-nav sticky-footer bg-dark" id="page-top">
  <!-- Navigation-->
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
    <a class="navbar-brand" href="halaman_utama.php">Toko Sepatu</a>
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav navbar-sidenav" id="exampleAccordion">
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Dashboard">
          <a class="nav-link" href="halaman_utama.php">
            <i class="fa fa-fw fa-dashboard"></i>
            <span class="nav-link-text">Dashboard</span>
          </a>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Charts">
          <a class="nav-link" href="transaksi.php">
            <i class="fa fa-fw fa-area-chart"></i>
            <span class="nav-link-text">Transaksi Baru</span>
          </a>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Charts">
          <a class="nav-link" href="daftar_transaksi.php">
            <i class="fa fa-fw fa-area-chart"></i>
            <span class="nav-link-text">Daftar Transaksi</span>
          </a>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Charts">
          <a class="nav-link" href="data_barang.php">
            <i class="fa fa-fw fa-area-chart"></i>
            <span class="nav-link-text">Data Barang</span>
          </a>
        </li>
        
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Components">
          <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseComponents" data-parent="#exampleAccordion">
            <i class="fa fa-fw fa-area-chart"></i>
            <span class="nav-link-text">Laporan Transaksi</span>
          </a>
          <ul class="sidenav-second-level collapse" id="collapseComponents">
            <li>
              <a href="laporan_transaksi_hari.php">Laporan Per Hari</a>
            </li>
            <li>
              <a href="cards.html">Laporan Per Bulan</a>
            </li>
            <li>
              <a href="cards.html">Laporan Per Tahun</a>
            </li>
            <li>
              <a href="laporan_transaksi_global.php">Laporan Keseluruhan</a>
            </li>
          </ul>
        </li>

     

        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Link">
          <a class="nav-link" href="#">          
          </a>
        </li>
      </ul>
      <ul class="navbar-nav sidenav-toggler">
        <li class="nav-item">
          <a class="nav-link text-center" id="sidenavToggler">
            <i class="fa fa-fw fa-angle-left"></i>
          </a>
        </li>
      </ul>
      <ul class="navbar-nav ml-auto">
          <li class="nav-item">
          <form class="form-inline my-2 my-lg-0 mr-lg-2">
            <div class="input-group">
              <input class="form-control" type="text" placeholder="Search for...">
              <span class="input-group-btn">
                <button class="btn btn-primary" type="button">
                  <i class="fa fa-search"></i>
                </button>
              </span>
            </div>
          </form>
        </li>
        
        <li class="nav-item">
          <a class="nav-link" data-toggle="modal" data-target="#exampleModal">
            <i class="fa fa-fw fa-sign-out"></i>Logout</a>
        </li>

      </ul>
    </div>
  </nav>

  <div class="content-wrapper">
    <div class="container-fluid">
    
      <!-- Breadcrumbs-->
     
      <!-- Icon Cards-->

      <!-- Area Chart Example-->
      <div class="card mb-3">
        <div class="card-header">
            <h1 align="center"> Daftar Transaksi </h1>
            <br>
            <hr>
           
            <!-- button -->
          <div class="container-fluid">
          <?php
            
            
          /*  if(isset($_POST['cari'])){
              $cari=$_POST['cari'];
              $cari=preg_replace("#[^a-z0-9]#i"," ", $cari);
              $data_pencarian ="select
                                    transaksi.id_transaksi,
                                    transaksi.tgl as tgl,
                                    detail_transaksi.total,
                                    detail_transaksi.qty,
                                    barang.id_barang,
                                    barang.nama_barang,
                                    barang.harga
                                  from 
                                    transaksi 
                                    join detail_transaksi on transaksi.id_transaksi = detail_transaksi.id_transaksi
                                    join barang on detail_transaksi.id_barang = barang.id_barang
                                  where tgl = '%$cari%'
                  
                ";
               
                 $res= mysqli_query($link, $data_pencarian);
                 $row = mysqli_num_rows($res);
                 // var_dump($res);
              
          }else{
            
          }
        */


            include('../include/config.php');
            $link=koneksi_db(); 
            $sql="select
                    transaksi.id_transaksi,
                    transaksi.tgl as tgl,
                    detail_transaksi.total,
                    detail_transaksi.qty,
                    barang.id_barang,
                    barang.nama_barang,
                    barang.harga
                  from 
                    transaksi 
                    join detail_transaksi on transaksi.id_transaksi = detail_transaksi.id_transaksi
                    join barang on detail_transaksi.id_barang = barang.id_barang
                  
                ";
                //var_dump($sql);
                //$fcari   = isset($_POST ['fieldcari']) ? $_POST ['fieldcari'] : "" ; 
                $keyword = isset($_POST['keyword']) ? $_POST['keyword'] : ""; 
                if(isset($_POST['btncari']) && $_POST['btncari']=="Cari") 
                    $sql=$sql." where tgl like '%$keyword%'";
                     //var_dump($keyword);
                    $sql.=" order by transaksi.tgl"; 
                    $res=mysqli_query($link,$sql); 
                      $banyakrecord=mysqli_num_rows($res); 
                        if($banyakrecord>0){ 
                          
            
            ?> 
          
            <form action="laporan_transaksi_hari.php" method="post">
                    <label>Masukkan Tanggal </label> 
                    <input  type="text"  name="keyword" value="<?php isset($_POST['keyword']) ? $_POST['keyword'] : ""?>"  placeholder="YYYY-MM-DD" required>
                    <input  type="submit" class="btn btn-primary" name="btncari" value="Cari"> 
            </form>

        <!--  <a href="proses_laporan_global.php"><button type="button" class="btn btn-primary" data-toggle="modal" data-whatever="@mdo">Cetak Laporan</button> </a>
          
          <button type="button" class="btn btn-primary" data-toggle="modal" data-whatever="@mdo">Laporan Transaksi Per Bulan</button>
          <button type="button" class="btn btn-primary" data-toggle="modal"  data-whatever="@mdo">Laporan Transaksi Per Tahun</button> --

          <!-- action="proses_transaksi.php" -->

                   
            <!-- ############### -->

             <!-- Tabel Data Barang-->
             <br>
             <br>

      <!--             -->
      

       <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i> Laporan Transaksi</div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable"  width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Tanggal</th>
                  <th>Kode Transaksi</th>
                  <th>Kode Barang</th>
                  <th>Nama Barang</th>
                  <th> QTY</th>
                  <th>Harga</th>
                  <th>Total Harga</th>
                  <!--<th>Aksi</th>-->
                </tr>
              </thead>
              
              <tbody>
              <?php
              
                   $i=0; 
                   while( $data=mysqli_fetch_array($res)){ 
                   $i++;
                   $total = $data['total'];
              ?>
                <tr>
                  <td><?php echo $i;?></td>
                  <td><?php echo $data['tgl'];?></td>
                  <td><?php echo $data['id_transaksi'];?></td>
                  <td><?php echo $data['id_barang'];?></td>
                  <td><?php echo $data['nama_barang'];?></td>
                  <td><?php echo $data['qty'];?></td>
                  <td><?php echo $data['harga'];?></td>
                  <td><?php echo $data['total'];?></td>
                  <!--<td><a href="detail.php?id_transaksi=<?php echo $data['id_barang'];?>"> Detail</a></td>-->

                </tr>
              </tbody> 
                                                                    
              <?php
            }

              ?>
            </table>
            <a href="proses_laporan_hari.php?id=<?php echo $keyword;?>"> <button type="button" class="btn btn-primary pull-right" data-whatever="@mdo" >Cetak</button></a>   
            <?php
              } else echo "Tidak ada data pada tabel Kategori."; 
            //}

            ?>
           

          </div>
        </div>
        <div class="card-footer small text-muted"></div>
      </div>
    </div>

    </div>
             
             <!-- Batas Data Barang--> 




      
        </div>  
      </div>
     
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
    <footer class="sticky-footer">
      <div class="container">
        <div class="text-center">
          <small>Copyright © Your Website 2017</small>
        </div>
      </div>
    </footer>
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fa fa-angle-up"></i>
    </a>
    <!-- Logout Modal-->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Apa Anda Yakin untuk Keluar? </h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">Tekan "Ya" untuk Keluar</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Tidak</button>
            <a class="btn btn-primary" href="index.php">Ya</a>
          </div>
        </div>
      </div>
    </div>
    <!-- Bootstrap core JavaScript-->
    <script src="../vendor/jquery/jquery.min.js"></script>
    <script src="../vendor/popper/popper.min.js"></script>
    <script src="../vendor/bootstrap/js/bootstrap.min.js"></script>
    <!-- Core plugin JavaScript-->
    <script src="../vendor/jquery-easing/jquery.easing.min.js"></script>
    <!-- Page level plugin JavaScript-->
    <script src="../vendor/chart.js/Chart.min.js"></script>
    <script src="../vendor/datatables/jquery.dataTables.js"></script>
    <script src="../vendor/datatables/dataTables.bootstrap4.js"></script>
    <!-- Custom scripts for all pages-->
    <script src="../js/sb-admin.min.js"></script>
    <!-- Custom scripts for this page-->
    <script src="../js/sb-admin-datatables.min.js"></script>
    <!-- <script src="../js/sb-admin-charts.min.js"></script> -->
  </div>




</script>
</body>


</html>
