<?php ob_start(); ?>
<html>
<head>
  <title>Cetak PDF</title>
    
   <style>
   table {border-collapse:collapse; table-layout:fixed;width: 800px;}
   table td {word-wrap:break-word;width: 12%;}
   </style>
</head>
<body>
  
<h1 style="text-align: center;">Data Transaksi</h1>
<table border="1" width="100%">
<tr>
  <th>No</th>
  <th>Tanggal</th>
  <th>Kode Transaksi</th>
  <th>Kode Barang</th>
  <th>Nama Barang</th>
  <th> QTY</th>
  <th>Harga</th>
  <th>Total Harga</th>
</tr>
<?php
// Load file koneksi.php
include "../include/config.php";
$link = koneksi_db();
$tgl = $_GET['id'];
$query = "
          select
                    transaksi.id_transaksi,
                    DATE_FORMAT(transaksi.tgl,'%d-%m-%Y') as tgl,
                    detail_transaksi.total,
                    detail_transaksi.qty,
                    barang.id_barang,
                    barang.nama_barang,
                    barang.harga
                  from 
                    transaksi 
                    join detail_transaksi on transaksi.id_transaksi = detail_transaksi.id_transaksi
                    join barang on detail_transaksi.id_barang = barang.id_barang
                  where tgl = '$tgl'
                  
          ";


           // Tampilkan semua data gambar
$sql = mysqli_query($link, $query); // Eksekusi/Jalankan query dari variabel $query
$row = mysqli_num_rows($sql); // Ambil jumlah data dari hasil eksekusi $sql
 //var_dump($row);
if($row > 0){ // Jika jumlah data lebih dari 0 (Berarti jika data ada)   where barang.id_barang = '$id_barang'
    $i=0;
    while($data = mysqli_fetch_array($sql)){ // Ambil semua data dari hasil eksekusi $sql
    $i++;
    $total = $data['total'];
        echo "<tr>";
        echo "<td>".$i."</td>";
        echo "<td>".$data['tgl']."</td>";
        echo "<td>".$data['id_transaksi']."</td>";
        echo "<td>".$data['id_barang']."</td>";
        echo "<td>".$data['nama_barang']."</td>";
        echo "<td>".$data['qty']."</td>";
        echo "<td>".$data['harga']."</td>";
        echo "<td>".$data['total']."</td>";
        echo "</tr>";
    }
}else{ // Jika data tidak ada
    echo "<tr><td colspan='4'>Data tidak ada</td></tr>";
}
?>
</table>

</body>
</html>

<?php
$html = ob_get_contents();
ob_end_clean();
        
require_once('../html2pdf/html2pdf.class.php');
$pdf = new HTML2PDF('P','A4','en');
$pdf->WriteHTML($html);
$pdf->Output('Data Transaksi Harian.pdf', 'D');
?>