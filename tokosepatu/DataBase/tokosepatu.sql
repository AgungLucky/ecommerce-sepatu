-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 09, 2017 at 08:39 AM
-- Server version: 10.1.10-MariaDB
-- PHP Version: 5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tokosepatu`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `username` varchar(30) NOT NULL,
  `userpass` varchar(30) DEFAULT NULL,
  `nama` varchar(30) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`username`, `userpass`, `nama`) VALUES
('adm', '123', ''),
('admin', 'admin', '');

-- --------------------------------------------------------

--
-- Table structure for table `barang`
--

CREATE TABLE `barang` (
  `id_barang` varchar(10) NOT NULL,
  `nama_barang` varchar(25) NOT NULL,
  `stok` int(5) DEFAULT NULL,
  `harga` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `barang`
--

INSERT INTO `barang` (`id_barang`, `nama_barang`, `stok`, `harga`) VALUES
('10113345', 'buku novel', 83, 100000),
('10113355', 'buku', 84, 30000),
('10113372', 'baju', 81, 120000),
('1021102', 'buku aja', 50, 120000);

-- --------------------------------------------------------

--
-- Table structure for table `detail_transaksi`
--

CREATE TABLE `detail_transaksi` (
  `id_detail_transaksi` int(11) NOT NULL,
  `id_transaksi` int(11) NOT NULL,
  `id_barang` varchar(10) NOT NULL,
  `qty` int(11) NOT NULL,
  `total` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_transaksi`
--

INSERT INTO `detail_transaksi` (`id_detail_transaksi`, `id_transaksi`, `id_barang`, `qty`, `total`) VALUES
(11, 1, '10113345', 1, 100000),
(12, 1, '10113372', 5, 600000),
(13, 1, '1021102', 5, 600000),
(14, 2, '10113355', 2, 60000),
(15, 2, '10113372', 4, 480000),
(16, 3, '10113355', 2, 60000),
(17, 4, '', 0, 0),
(18, 4, '10113345 ', 2, 200000),
(19, 5, '10113345 ', 2, 200000),
(20, 5, '10113355', 5, 150000),
(21, 6, '10113345 ', 2, 200000),
(22, 7, '10113345 ', 2, 200000),
(23, 8, '10113345 ', 1, 100000),
(24, 8, '10113345 ', 2, 200000),
(25, 9, '10113355 ', 2, 60000),
(26, 10, '10113372 ', 2, 240000),
(27, 11, '10113345 ', 1, 100000),
(28, 11, '10113355 ', 1, 30000),
(29, 11, '10113372 ', 1, 30000),
(30, 12, '1021102 ', 1, 120000),
(31, 13, '1021102 ', 1, 120000),
(32, 14, '10113345 ', 1, 100000),
(33, 14, '10113355  ', 1, 100000),
(34, 15, '10113345 ', 1, 100000),
(35, 16, '10113345 ', 2, 200000),
(36, 16, '10113372', 2, 240000);

-- --------------------------------------------------------

--
-- Table structure for table `lap_transaksi`
--

CREATE TABLE `lap_transaksi` (
  `id_lap` int(5) NOT NULL,
  `id_transaksi` int(5) DEFAULT NULL,
  `id_barang` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `transaksi`
--

CREATE TABLE `transaksi` (
  `id_transaksi` int(11) NOT NULL,
  `tgl` date NOT NULL,
  `total_harga` int(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaksi`
--

INSERT INTO `transaksi` (`id_transaksi`, `tgl`, `total_harga`) VALUES
(16, '2017-10-09', 440000),
(2, '2017-10-07', 540000),
(1, '2017-10-07', 1300000),
(4, '2017-10-08', 200000),
(6, '2017-10-09', 200000),
(7, '2017-10-09', 200000),
(8, '2017-10-09', 300000),
(9, '2017-10-09', 60000),
(10, '2017-10-09', 240000),
(11, '2017-10-09', 160000),
(12, '2017-10-09', 120000),
(13, '2017-10-09', 120000),
(14, '2017-10-09', 200000),
(15, '2017-10-09', 100000);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`username`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`id_barang`);

--
-- Indexes for table `detail_transaksi`
--
ALTER TABLE `detail_transaksi`
  ADD PRIMARY KEY (`id_detail_transaksi`);

--
-- Indexes for table `lap_transaksi`
--
ALTER TABLE `lap_transaksi`
  ADD PRIMARY KEY (`id_lap`);

--
-- Indexes for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`id_transaksi`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `detail_transaksi`
--
ALTER TABLE `detail_transaksi`
  MODIFY `id_detail_transaksi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT for table `transaksi`
--
ALTER TABLE `transaksi`
  MODIFY `id_transaksi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
